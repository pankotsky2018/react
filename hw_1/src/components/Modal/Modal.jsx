import React, { Component } from "react";
import s from "./Modal.module.scss";
import { close } from "../index.js";
import ModalButton from './../Buttons/ModalButton';

export class Modal extends Component {

	handleClose = () => {
		this.props.onClick(false);
	};

  handleOverflowClick = (e) => {
    e.stopPropagation();
    if(e.target === e.currentTarget){
      this.handleClose(false);
    }
  }

	render() {
		const { header, textContent, mainColor, headerColor } = this.props;
		const firstBtnText = 'Ok';
		const secondBtnText = 'Cancel'

		return (
			<div
        onClick={this.handleOverflowClick}
        className={s.modalWrapper}>
				<div
          className={s.modal}
          style={{ backgroundColor: `${mainColor}` }}>
					<div
						className={s.modalHeader}
						style={{ backgroundColor: `${headerColor}` }}
					>
						<p className={s.header}>{header}</p>
						<img
							className={s.close} 
							onClick={this.handleClose} 
							src={close} 
							alt="img" />
					</div>
					<div className={s.modalContent}>
						<p>{textContent}</p>
					</div>
					<div className={s.buttonWrapper}>
						<ModalButton btnText={firstBtnText}/>
						<ModalButton btnText={secondBtnText}/>
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;
