import React, { Component } from "react";
import s from "./ModalButton.module.scss";

class ModalButton extends Component {
	render() {
		const { btnText } = this.props;

		return (
			<div>
				<button className={s.modalButton}>{btnText}</button>
			</div>
		);
	}
}

export default ModalButton;
