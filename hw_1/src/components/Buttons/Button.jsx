import { Component } from "react";
import s from "./Button.module.scss"

class Button extends Component {

    render () {
        const { bgColor, text, onClick } = this.props;

        return (

            <div>
            <button 
                onClick={onClick}
                className={s.button}
                style={{backgroundColor: `${bgColor}`}}
                >
                {text}
            </button>
            </div> 
        )
    };
};

export default Button;