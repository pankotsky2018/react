import { Component } from "react";
import Modal from "../Modal/Modal";
import s from "./Main.module.scss";

class Main extends Component {
	render() {
		const textFirstModal =
			"Once you save this file,it won't to be possible to undo this action. Are you sure you want to save it?";
		const textSecondModal =
			"Once you delete this file,it won't to be possible to undo this action. Are you sure you want to delete it?";
		const headerFirstModal = "Do you want to SAVE this file?";
		const headerSecondModal = "Do you want to DELETE this file?";
		const { handleClickFirstButton, handleClickSecondButton } = this.props;
		return (
			<main className={s.main}>
				{!!this.props.isOpenedFirstModal && (
					<Modal
						onClick={handleClickFirstButton}
						headerColor="#4cbd4c"
						mainColor="#5FD65E"
						header={headerFirstModal}
						textContent={textFirstModal}
					/>
				)}
				{!!this.props.isOpenedSecondModal && (
					<Modal
						onClick={handleClickSecondButton}
						headerColor="#d44637"
						mainColor="#e74c3c"
						header={headerSecondModal}
						textContent={textSecondModal}
					/>
				)}
			</main>
		);
	}
}

export default Main;
