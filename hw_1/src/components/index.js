
// import Button from './Button/index';
// import Main from './Main/index';
// import Header from './Header/index';
// import Modal from './Modal/index';

// export default Button;
// export default Main;
// export default Header;
// export default Modal;

export {default as Button} from "./Buttons/index";
export {default as Main} from "./Main/index";
export {default as Header} from "./Header/index";
export {default as Modal} from "./Modal/index";
export {default as close } from "../img/close.png"
export {default as ModalButton} from "./Buttons/index"
 