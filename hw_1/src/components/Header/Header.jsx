import { Component } from "react";
import { Button } from "../index";
import s from "./Header.module.scss";

class Header extends Component {
	render() {
		const { handleClickFirstButton, handleClickSecondButton } = this.props;

		return (
			<header className={s.header}>
				<Button
					onClick={handleClickFirstButton}
					bgColor="#5FD65E"
					text="Open first modal"
				/>
				<Button
					onClick={handleClickSecondButton}
					bgColor="#e74c3c"
					text="Open second modal"
				/>
			</header>
		);
	}
}

export default Header;
