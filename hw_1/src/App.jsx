import './App.css';
import { Component } from 'react';
import { Main, Header} from './components/index.js';

class App extends Component{
 
  constructor () {
      super ()
      this.state = {
        isOpenedFirstModal: false,
        isOpenedSecondModal: false
      }
  }

  handleClickFirstButton = (state = true) => {
      this.setState({isOpenedFirstModal: state});
  }

  handleClickSecondButton = (state = true) => {
    this.setState({isOpenedSecondModal: state});
  }

  
  
  render () {
    return (
      <div className="App">
        <Header 
          handleClickFirstButton={this.handleClickFirstButton}
          handleClickSecondButton={this.handleClickSecondButton}
        />        
        <Main 
          handleClickFirstButton={this.handleClickFirstButton}
          handleClickSecondButton={this.handleClickSecondButton}
          isOpenedFirstModal={this.state.isOpenedFirstModal}
          isOpenedSecondModal={this.state.isOpenedSecondModal}
        />
      </div>
    );
  }
}

export default App;
