import { Component } from "react";
import s from "./ButtonCard.module.scss"

class Button extends Component {

    render () {
        const { text, onClick } = this.props;

        return (

            <div>
            <button 
                onClick={onClick}
                className={s.button}
                >
                {text}
            </button>
            </div> 
        )
    };
};

export default Button;