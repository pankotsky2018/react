import React, { Component } from "react";
import s from "./ModalButton.module.scss";
import correct from "../../img/correct.png";

class ModalButton extends Component {
	constructor() {
		super();
		this.state = {
			added: false,
		};
	}

	handleClick = (itemCart) => {
		if (this.props.cart.find((item) => item.id === itemCart.id)) {
			this.props.removeCart(itemCart);
		} else {
			this.props.addCart(itemCart);
		}
	};

	render() {
		const { btnText, itemCart, cart } = this.props;

		return (
			<div>
				<button
					onClick={() => this.handleClick(itemCart)}
					className={s.modalButton}
				>
					{this.state.added || cart.find((item) => item.id === itemCart.id) ? (
						<img src={correct} className={s.img} alt="done" />
					) : (
						btnText
					)}
				</button>
			</div>
		);
	}
}

export default ModalButton;
