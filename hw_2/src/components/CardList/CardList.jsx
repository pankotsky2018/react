import React, { Component } from "react";
import Card from "../Card/index";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

import s from "./CardList.module.scss";

export default class CardList extends Component {
	render() {
		const {
			handleClickButtonCard,
			cards,
			favoriteCards,
			addFavoriteCards,
			removeFavoriteCard,
			isOpenedModal,
			addItemCart,
			itemCart,
			addCart,
			removeCart,
			cart,
		} = this.props;

		const headerModal = "Додати у кошик?";

		return (
			<div className={s.cardWrapper}>
				{cards.map((card) => {
					return (
						<Card
							card={card}
							key={card.id}
							favoriteCards={favoriteCards}
							addFavoriteCards={addFavoriteCards}
							removeFavoriteCard={removeFavoriteCard}
							handleClickButtonCard={handleClickButtonCard}
							isOpenedModal={isOpenedModal}
							addItemCart={addItemCart}
						/>
					);
				})}
				{!!isOpenedModal && (
					<Modal
						handleClickButtonCard={handleClickButtonCard}
						header={headerModal}
						itemCart={itemCart}
						addCart={addCart}
						removeCart={removeCart}
						cart={cart}
						// textContent={}
					/>
				)}
			</div>
		);
	}
}


CardList.propTypes = {
	cards: PropTypes.array
}

