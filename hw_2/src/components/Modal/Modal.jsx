import React, { Component } from "react";
import s from "./Modal.module.scss";
import { close } from "../index.js";
import ModalButton from "./../Buttons/ModalButton";

export class Modal extends Component {
	handleClose = () => {
		this.props.handleClickButtonCard(false, "auto");

	};

	handleOverflowClick = (e) => {
		e.stopPropagation();
		if (e.target === e.currentTarget) {
			this.handleClose();
		}
	};

	render() {
		const { header, textContent, addCart, itemCart, cart, removeCart } =
			this.props;
		const firstBtnText = "Add";
		// const secondBtnText = "Go to Cart";

		return (
			<div onClick={this.handleOverflowClick} className={s.modalWrapper}>
				<div className={s.modal}>
					<div className={s.modalHeader}>
						<p className={s.header}>{header}</p>
						<img
							className={s.close}
							onClick={this.handleClose}
							src={close}
							alt="img"
						/>
					</div>
					<div className={s.modalContentWrapper}>
						<div className={s.modalContent}>
							{/* <p>{textContent}</p> */}
							<div className={s.cardWrapper}>
								<img className={s.img} src={itemCart.url} alt="sneakers" />
								<div className={s.description}>
									<p className={s.name}>Кросівки {itemCart.name}</p>
									<div className={s.wrapper}>
										<p className={s.price}>{itemCart.price} грн</p>
									</div>
								</div>
							</div>
						</div>
						<div className={s.buttonWrapper}>
							<ModalButton
								cart={cart}
								itemCart={itemCart}
								addCart={addCart}
								removeCart={removeCart}
								btnText={firstBtnText}
							/>
							{/* <ModalButton btnText={secondBtnText} /> */}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;
