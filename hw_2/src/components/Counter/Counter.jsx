import React, { Component } from "react";
import s from "./Counter.module.scss"




export default class Counter extends Component {


  render() {
    const { counter } = this.props;

    return <div className={s.counter}>{counter}</div>;
  }
}
