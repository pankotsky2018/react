import { Component } from "react";
import CardList from "./../CardList/index";
import PropTypes from "prop-types";
import s from "./Main.module.scss";

class Main extends Component {
	constructor() {
		super();

		this.state = {
			cards: [],
		};
	}

	getCard = async (url) => {
		const request = await fetch(url);
		const res = await request.json();
		this.setState({ cards: res });
	};

	componentDidMount() {
		this.getCard("/products.json");
	}

	render() {
		// const textModal = "Додати у кошик?";
		const {
			handleClickButtonCard,
			addFavoriteCards,
			favoriteCards,
			removeFavoriteCard,
			isOpenedModal,
			addItemCart,
			itemCart,
			addCart,
			removeCart,
			cart,
		} = this.props;
		const { cards } = this.state;

		return (
			<main className={s.main}>
				<CardList
					isOpenedModal={isOpenedModal}
					cards={cards}
					handleClickButtonCard={handleClickButtonCard}
					favoriteCards={favoriteCards}
					addFavoriteCards={addFavoriteCards}
					removeFavoriteCard={removeFavoriteCard}
					addItemCart={addItemCart}
					itemCart={itemCart}
					addCart={addCart}
					removeCart={removeCart}
					cart={cart}
				/>
			</main>
		);
	}
}

export default Main;


Main.propTypes = {
	favoriteCards: PropTypes.array,
	isOpenedModal: PropTypes.bool,
	itemCart: PropTypes.object,
	cart: PropTypes.array,	
};