import { Component } from "react";
import Counter from "./../Counter/Counter";
import PropTypes from "prop-types";
import s from "./Header.module.scss";
import { ReactComponent as Favorite } from "../../img/favorite.svg"
import { ReactComponent as Cart } from "../../img/cart.svg"


class Header extends Component {
	render() {
		const { counterFavorites, counterCart } = this.props;

		return (
			<header className={s.header}>
				<div className={s.svgWrapper}>
					<div className={s.favoriteWrapper}>
						<Favorite />
						{counterFavorites !== 0 && (
							<div className={s.counter}>
								<Counter counter={counterFavorites} />
							</div>
						)}
					</div>
					<div className={s.cartWrapper}>
						<Cart />
						{counterCart !== 0 && (
							<div className={s.counter}>
								<Counter counter={counterCart} />
							</div>
						)}
					</div>
				</div>
			</header>
		);
	}
}

export default Header;

Header.propTypes = {
	counterFavorites: PropTypes.number,
	counterCart: PropTypes.number,
};

Header.defaultProps = {
	counterFavorites: 0,
	counterCart: 0,
}