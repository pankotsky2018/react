import React, { Component, createRef } from "react";
import Button from "./../Buttons/index";
import s from "./Card.module.scss";

class Card extends Component {
	constructor(props) {
		super(props);
		this.svg = createRef();
	}

	handleFavoriteSvg = () => {
		if (
			!!this.props.favoriteCards.find((item) => item.id === this.props.card.id)
		) {
			return { fill: "#2f4f4f" };
		} else {
			return { fill: "" };
		}
	};

	handleFavoriteIcon = () => {
		if (
			!!this.props.favoriteCards.find((item) => item.id === this.props.card.id)
		) {
			return {
				background:
					"radial-gradient(circle, rgba(0, 255, 238, 0.36) 42%, rgba(255, 255, 255, 0) 54%)",
			};
		} else {
			return { background: "" };
		}
	};

	handleFavoriteCard = (e) => {
		if (
			!this.props.favoriteCards.find((item) => item.id === this.props.card.id)
		) {
			e.currentTarget.style =
				"background: radial-gradient(circle, rgba(0, 255, 238, 0.36) 42%, rgba(255, 255, 255, 0) 54%)";
			this.svg.current.style = "fill: #2f4f4f";
			this.props.addFavoriteCards(this.props.card);
		} else {
			e.currentTarget.style = "";
			this.svg.current.style = "";
			this.props.removeFavoriteCard(this.props.card);
		}
	};

	render() {
		const { card, handleClickButtonCard, addItemCart } = this.props;

		return (
			<>
				<div className={s.cardWrapper}>
					<img className={s.img} src={card.url} alt="sneakers" />
					<div className={s.description}>
						<p className={s.name}>Кросівки {card.name}</p>
						<div className={s.wrapper}>
							<p className={s.price}>{card.price} грн</p>
							<p className={s.oldPrice}>
								{!!card.oldPrice && `${card.oldPrice} грн`}
							</p>
							<Button
								onClick={() => {
									handleClickButtonCard(true, "hidden");
									addItemCart(card);
								}}
								text="Add to Cart"
							/>
						</div>
						<p className={s.color}>Кольорів: {card.color}</p>
						<div
							onClick={this.handleFavoriteCard}
							className={s.svgWrapper}
							style={this.handleFavoriteIcon()}
						>
							<svg
								ref={this.svg}
								className={s.svg}
								width="30"
								height="30"
								viewBox="0 0 25 25"
								fill="none"
								xmlns="http://www.w3.org/2000/svg"
								style={this.handleFavoriteSvg()}
							>
								<path d="M12 2C10.0222 2 8.08879 2.58649 6.4443 3.6853C4.79981 4.78412 3.51809 6.3459 2.76121 8.17317C2.00433 10.0004 1.8063 12.0111 2.19215 13.9509C2.578 15.8907 3.53041 17.6725 4.92894 19.0711C6.32746 20.4696 8.10929 21.422 10.0491 21.8079C11.9889 22.1937 13.9996 21.9957 15.8268 21.2388C17.6541 20.4819 19.2159 19.2002 20.3147 17.5557C21.4135 15.9112 22 13.9778 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7363 6.95991 19.9997 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C10.4178 20 8.87104 19.5308 7.55544 18.6518C6.23985 17.7727 5.21447 16.5233 4.60897 15.0615C4.00347 13.5997 3.84504 11.9911 4.15372 10.4393C4.4624 8.88743 5.22433 7.46197 6.34315 6.34315C7.46197 5.22433 8.88743 4.4624 10.4393 4.15372C11.9911 3.84504 13.5997 4.00346 15.0615 4.60896C16.5233 5.21447 17.7727 6.23984 18.6518 7.55544C19.5308 8.87103 20 10.4177 20 12C19.9976 14.121 19.154 16.1544 17.6542 17.6542C16.1545 19.154 14.121 19.9976 12 20ZM12.958 10.8232L12 7.875L11.042 10.8232H7.942L10.45 12.6454L9.4919 15.5938L12 13.7716L14.5081 15.5938L13.55 12.6454L16.058 10.8232H12.958Z" />
							</svg>
						</div>
					</div>
				</div>
			</>
		);
	}
}

export default Card;
