import "./App.css";
import { Component } from "react";
import { Main, Header } from "./components/index.js";


class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpenedModal: false,
			favoriteCards: [],
			cart: [],
			itemCart: {},
		};
	}

	handleClickButtonCard = (state, value) => {
		this.setState({ isOpenedModal: state });
		document.body.style.overflow = value;
	};

	addFavoriteCards = (card) => {
		this.setState((prev) => {
			return { favoriteCards: [...prev.favoriteCards, card] };
		});
	};

	removeFavoriteCard = (card) => {
		this.setState((prev) => {
			return {
				favoriteCards: prev.favoriteCards.filter((item) => item.id !== card.id),
			};
		});
	};

	addCart = (card) => {
		this.setState((prev) => {
			return { cart: [...prev.cart, card] };
		});
	};

	removeCart = (card) => {
		this.setState((prev) => {
			return { cart: prev.cart.filter((item) => item.id !== card.id) };
		});
	};

	addItemCart = (item) => {
		this.setState({ itemCart: item });
	};

	componentDidUpdate() {
		window.localStorage.setItem(
			"favoriteCards",
			JSON.stringify(this.state.favoriteCards)
		);
		window.localStorage.setItem("cart", JSON.stringify(this.state.cart));
	}

	componentDidMount() {
		if (window.localStorage.getItem("favoriteCards") !== null) {
			this.setState({
				favoriteCards: JSON.parse(window.localStorage.getItem("favoriteCards")),
			});
		}
		if (window.localStorage.getItem("cart") !== null) {
			this.setState({
				cart: JSON.parse(window.localStorage.getItem("cart")),
			});
		}
	}

	render() {
		const { isOpenedModal, itemCart, cart, favoriteCards } = this.state;

		return (
			<div className="App">
				<Header
					counterFavorites={favoriteCards.length}
					counterCart={cart.length}
				/>
				<Main
					favoriteCards={favoriteCards}
					addFavoriteCards={this.addFavoriteCards}
					removeFavoriteCard={this.removeFavoriteCard}
					handleClickButtonCard={this.handleClickButtonCard}
					isOpenedModal={isOpenedModal}
					itemCart={itemCart}
					addItemCart={this.addItemCart}
					addCart={this.addCart}
					removeCart={this.removeCart}
					cart={cart}
				/>
			</div>
		);
	}
}

export default App;