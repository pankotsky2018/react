import React, { createRef } from "react";

import { Button } from "../index";
import { ReactComponent as FavSvg } from "../../img/favorite.svg";

import s from "./Card.module.scss";

const Card = ({
	card,
	handleClickButtonCard,
	addItemCart,
	favoriteCards,
	addFavoriteCards,
	removeFavoriteCard,
}) => {
	const svg = createRef();

	const handleFavoriteSvg = () => {
		if (!!favoriteCards.find((item) => item.id === card.id)) {
			return { fill: "#2f4f4f" };
		} else {
			return { fill: "" };
		}
	};

	const handleFavoriteIcon = () => {
		if (!!favoriteCards.find((item) => item.id === card.id)) {
			return {
				background:
					"radial-gradient(circle, rgba(0, 255, 238, 0.36) 42%, rgba(255, 255, 255, 0) 54%)",
			};
		} else {
			return { background: "" };
		}
	};

	const handleFavoriteCard = (e) => {
		if (!favoriteCards.find((item) => item.id === card.id)) {
			e.currentTarget.style =
				"background: radial-gradient(circle, rgba(0, 255, 238, 0.36) 42%, rgba(255, 255, 255, 0) 54%)";
			svg.current.style = "fill: #2f4f4f";
			addFavoriteCards(card);
		} else {
			e.currentTarget.style = "";
			svg.current.style = "";
			removeFavoriteCard(card);
		}
	};
	return (
		<>
			<div className={s.cardWrapper}>
				<img className={s.img} src={card.url} alt="sneakers" />
				<div className={s.description}>
					<p className={s.name}>Кросівки {card.name}</p>
					<div className={s.wrapper}>
						<p className={s.price}>{card.price} грн</p>
						<p className={s.oldPrice}>
							{!!card.oldPrice && `${card.oldPrice} грн`}
						</p>
						<Button
							className={s.buttonCard}
							onClick={() => {
								handleClickButtonCard(true, "hidden");
								addItemCart(card);
							}}
							text="Add to Cart"
						/>
					</div>
					<p className={s.color}>Кольорів: {card.color}</p>
					<div
						onClick={handleFavoriteCard}
						className={s.svgWrapper}
						style={handleFavoriteIcon()}
					>
						<FavSvg
							ref={svg}
							className={s.svg}
							width="30"
							height="30"
							style={handleFavoriteSvg()}
						/>
					</div>
				</div>
			</div>
		</>
	);
};

export default Card;
