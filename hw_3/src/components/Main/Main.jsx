import { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { Router } from "./../../routes/Router";

import s from "./Main.module.scss";

const Main = ({
	handleClickButtonCard,
	addFavoriteCards,
	favoriteCards,
	removeFavoriteCard,
	isOpenedModal,
	addItemCart,
	itemCart,
	addCart,
	removeCart,
	cart,
	counterCart,
	counterFavorites,
}) => {
	const [cards, setCards] = useState([]);

	const getCard = async (url) => {
		const request = await fetch(url);
		const res = await request.json();
		setCards(res);
	};

	useEffect(() => {
		getCard("/products.json");
	}, []);

	return (
		<main className={s.main}>
			<Router
				favoriteCards={favoriteCards}
				addFavoriteCards={addFavoriteCards}
				removeFavoriteCard={removeFavoriteCard}
				handleClickButtonCard={handleClickButtonCard}
				isOpenedModal={isOpenedModal}
				itemCart={itemCart}
				addItemCart={addItemCart}
				addCart={addCart}
				removeCart={removeCart}
				cart={cart}
				counterCart={counterCart}
				counterFavorites={counterFavorites}
				cards={cards}
			/>
		</main>
	);
};

export default Main;

Main.propTypes = {
	favoriteCards: PropTypes.array,
	isOpenedModal: PropTypes.bool,
	itemCart: PropTypes.object,
	cart: PropTypes.array,
};
