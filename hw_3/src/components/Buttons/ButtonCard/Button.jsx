import React from "react";
// import s from "./Button.module.scss";

const Button = ({ text, onClick, className }) => {
	return (
		<div>
			<button onClick={onClick} className={className}>
				{text}
			</button>
		</div>
	);
};

export default Button;
