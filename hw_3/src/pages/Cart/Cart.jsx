import React from "react";

import { Button, CartCard, Modal } from "../../components/index";

import s from "./Cart.module.scss"


const Cart = ({ 
  counterCart, 
  cart, 
  removeCart, 
  isOpenedModal, 
  handleClickButtonCard,
  itemCart,
  addItemCart,
 }) => {

  const modalHeader = "Ви дійсно хочете видалити цей товар?"
  const firstButtonContent = "Так";
  const secondButtonContent = "Ні";


  return (
    <div className={s.cart}>
        {counterCart === 0
            ? <h3 className={s.title}>Кошик порожній &#128542;</h3>
            : <h3 className={s.title}>Товарів у корзині: {counterCart} &#128525;</h3>}
        <div className={s.cardWrapper}>
        {cart.map((card) => {
            return(
            <CartCard
                card={card}
                key={card.id}
                handleClickButtonCard={handleClickButtonCard}
                addItemCart={addItemCart}
            />
        )})}
        </div>
        {!!isOpenedModal && (
          <Modal
              removeCart={removeCart}
              header={modalHeader} 
              handleClickButtonCard={handleClickButtonCard}
          >
              <Button 
                  className={s.buttonYes}
                  text={firstButtonContent}
                  onClick={() => {
                    removeCart(itemCart);
                    handleClickButtonCard(false, 'auto');
                  }}
              />
              <Button
                  className={s.buttonNo}
                  text={secondButtonContent}
                  onClick={() => {
                    handleClickButtonCard(false, 'auto');
                  }}
              />
          </Modal>
        )}
    </div>
  );
};

export default Cart;

