import { useState, useEffect } from "react";
import { Main, Header } from "./components/index.js";
import "./App.css";

const App = () => {
	const [isOpenedModal, setIsOpenedModal] = useState(false);
	const [favoriteCards, setFavoriteCards] = useState([]);
	const [cart, setCart] = useState([]);
	const [itemCart, setItemCart] = useState({});

	const handleClickButtonCard = (state, value) => {
		setIsOpenedModal(state);
		document.body.style.overflow = value;
	};

	const addFavoriteCards = (card) => {
		setFavoriteCards((state) => {
			return [...state, card];
		});
	};

	const removeFavoriteCard = (card) => {
		setFavoriteCards((prev) => {
			return prev.filter((item) => item.id !== card.id);
		});
        // window.localStorage.removeItem("favoriteCards");
	};

	const addCart = (card) => {
		setCart((prev) => {
			return [...prev, card];
		});
	};

	const removeCart = (card) => {
		setCart((prev) => {
			return prev.filter((item) => item.id !== card.id);
		});
        // window.localStorage.removeItem("cart");
	};

	const addItemCart = (item) => {
		setItemCart(item);
	};

	useEffect(() => {
		if (window.localStorage.getItem("favoriteCards") !== null) {
			setFavoriteCards(JSON.parse(window.localStorage.getItem("favoriteCards")));
		}
		if (window.localStorage.getItem("cart") !== null) {
			setCart(JSON.parse(window.localStorage.getItem("cart")));
		}
	}, []);

	useEffect(() => {
		window.localStorage.setItem("favoriteCards", JSON.stringify(favoriteCards));
		window.localStorage.setItem("cart", JSON.stringify(cart));
	}, [favoriteCards, cart]);


	// window.localStorage.clear()
	return (<>
		<div className="App">
			<Header
				counterFavorites={favoriteCards.length}
				counterCart={cart.length}
			/>
			<Main
				favoriteCards={favoriteCards}
				addFavoriteCards={addFavoriteCards}
				removeFavoriteCard={removeFavoriteCard}
				handleClickButtonCard={handleClickButtonCard}
				isOpenedModal={isOpenedModal}
				itemCart={itemCart}
				addItemCart={addItemCart}
				addCart={addCart}
				removeCart={removeCart}
				cart={cart}
				counterCart={cart.length}
				counterFavorites={favoriteCards.length}
			/>
		</div>
	</>);
};

export default App;
