import React from "react";

import { close } from "../index.js";

import s from "./Modal.module.scss";

const Modal = ({ handleClickButtonCard, header, children }) => {
	const handleClose = () => {
		handleClickButtonCard(false, "auto");
	};

	const handleOverflowClick = (e) => {
		e.stopPropagation();
		if (e.target === e.currentTarget) {
			handleClose();
		}
	};

	return (
		<div onClick={handleOverflowClick} className={s.modalWrapper}>
			<div className={s.modal}>
				<div className={s.modalHeader}>
					<p className={s.header}>{header}</p>
					<img
						className={s.close}
						onClick={handleClose}
						src={close}
						alt="img"
					/>
				</div>
				<div className={s.modalContentWrapper}>{children}</div>
			</div>
		</div>
	);
};

export default Modal;
