import React from "react";

import close from "../../img/closeBlack.png";

import s from "./CartCard.module.scss";

const CartCard = ({ card, handleClickButtonCard, addItemCart }) => {
	return (
		<>
			<div className={s.cardWrapper}>
				<img className={s.img} src={card.url} alt="sneakers" />
				<div className={s.description}>
					<p className={s.name}>Кросівки {card.name}</p>
					<div className={s.wrapper}>
						<p className={s.price}>{card.price} грн</p>
						<p className={s.oldPrice}>
							{!!card.oldPrice && `${card.oldPrice} грн`}
						</p>
					</div>
					<div
						onClick={() => {
							handleClickButtonCard(true, "hidden");
							addItemCart(card)
						}}
						className={s.closeWrapper}
					>
						<img
							src={close}
							alt="close"
							className={s.close}
						/>
					</div>
				</div>
			</div>
		</>
	);
};

export default CartCard;
