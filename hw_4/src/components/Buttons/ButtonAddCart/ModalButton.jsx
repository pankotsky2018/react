import React from "react";
import correct from "../../../img/correct.png";
import s from "./ModalButton.module.scss";

const ModalButton = ({ itemCart, cart, removeCart, addCart }) => {

	const handleClick = (itemCart) => {
		if (cart.find((item) => item.id === itemCart.id)) {
			removeCart(itemCart);
		} else {
			addCart(itemCart);
		}
	};

		return (
			<div>
				<button
					onClick={() => handleClick(itemCart)}
					className={s.modalButton}
				>
					{cart.find((item) => item.id === itemCart.id) ? (
						<img src={correct} className={s.img} alt="done" />
					) : (
						"Додати"
					)}
				</button>
			</div>
		);
}

export default ModalButton;
