import Counter from "./../Counter/index";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { ReactComponent as FavSvg } from "../../img/favorite.svg";
import { ReactComponent as CartSvg } from "../../img/cart.svg";
import logo from "../../img/logo.png";

import s from "./Header.module.scss";

const Header = ({ counterFavorites, counterCart }) => {
	return (
		<header className={s.header}>
			<div className={s.wrapper}>
				<Link className={s.logoLink} to="/">
					<img className={s.logo} src={logo} alt="logo" />
				</Link>
				<div className={s.svgWrapper}>
					<div className={s.favoriteWrapper}>
						<Link className={s.favoriteLink} to="/favorites">
							<FavSvg />
							{counterFavorites !== 0 && (
								<div className={s.counter}>
									<Counter counter={counterFavorites} />
								</div>
							)}
						</Link>
					</div>
					<div className={s.cartWrapper}>
						<Link className={s.cartLink} to="/cart">
							<CartSvg />
							{counterCart !== 0 && (
								<div className={s.counter}>
									<Counter counter={counterCart} />
								</div>
							)}
						</Link>
					</div>
				</div>
			</div>
		</header>
	);
};

export default Header;

Header.propTypes = {
	counterFavorites: PropTypes.number,
	counterCart: PropTypes.number,
};

Header.defaultProps = {
	counterFavorites: 0,
	counterCart: 0,
};
