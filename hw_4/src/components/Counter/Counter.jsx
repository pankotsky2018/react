import s from "./Counter.module.scss";

const Counter = ({ counter }) => {
	return <div className={s.counter}>{counter}</div>;
};

export default Counter;
