import React from "react";

import { FavoriteCard } from "../../components/index";

import s from "./Favorites.module.scss";

const Favorites = ({ favoriteCards, counterFavorites, addFavoriteCards, removeFavoriteCard }) => {
	return <div>
        {counterFavorites === 0
            ? <h3 className={s.title}>Товари відсутні &#128542;</h3>
            : <h3 className={s.title}>Улюблених товарів: {counterFavorites} &#128525;</h3>}
        <div className={s.cardWrapper}>
            {favoriteCards.map(card => {
                return (
                <FavoriteCard 
                    card={card}
                    favoriteCards={favoriteCards}
                    addFavoriteCards={addFavoriteCards}
                    removeFavoriteCard={removeFavoriteCard}
                />)
            })}
        </div>
    </div>;
};

export default Favorites;
