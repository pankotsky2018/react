import React from "react";
import { Link } from "react-router-dom";
import { ModalButton, Modal, Button, Card } from "../../components/index";
import PropTypes from "prop-types";

import s from "./CardList.module.scss";

const CardList = ({
	handleClickButtonCard,
	cards,
	favoriteCards,
	addFavoriteCards,
	removeFavoriteCard,
	isOpenedModal,
	addItemCart,
	itemCart,
	addCart,
	removeCart,
	cart,
}) => {
	const headerModal = "Додати у кошик?";
	const buttonContent = <>До корзини &#8594;</>;

	const modalContent = (
		<>
			<div className={s.modalContent}>
				<div>
					<img className={s.img} src={itemCart.url} alt="sneakers" />
					<div className={s.description}>
						<p className={s.name}>Кросівки {itemCart.name}</p>
						<div className={s.wrapper}>
							<p className={s.price}>{itemCart.price} грн</p>
							<p className={s.oldPrice}>
								{!!itemCart.oldPrice && `${itemCart.oldPrice} грн`}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div className={s.buttonWrapper}>
				<ModalButton
					cart={cart}
					itemCart={itemCart}
					addCart={addCart}
					removeCart={removeCart}
				/>
				<Link to="/cart" onClick={() => {
					handleClickButtonCard(false, "auto");
				}}>
					<Button className={s.buttonGoCart} text={buttonContent} />
				</Link>
			</div>
		</>
	);

	return (
		<div className={s.cardWrapper}>
			{cards.map((card) => {
				return (
					<Card
						card={card}
						key={card.id}
						favoriteCards={favoriteCards}
						addFavoriteCards={addFavoriteCards}
						removeFavoriteCard={removeFavoriteCard}
						handleClickButtonCard={handleClickButtonCard}
						addItemCart={addItemCart}
					/>
				);
			})}
			{!!isOpenedModal && (
				<Modal
					handleClickButtonCard={handleClickButtonCard}
					header={headerModal}
					itemCart={itemCart}
					addCart={addCart}
					removeCart={removeCart}
					cart={cart}
				>
					{modalContent}
				</Modal>
			)}
		</div>
	);
};

CardList.propTypes = {
	cards: PropTypes.array,
};

export default CardList;
