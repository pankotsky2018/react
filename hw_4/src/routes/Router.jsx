import { Switch, Route } from "react-router-dom";
import { CardList, Cart, Favorites } from "../pages/index";

export const Router = ({
	isOpenedModal,
	cards,
	handleClickButtonCard,
	favoriteCards,
	addFavoriteCards,
	removeFavoriteCard,
	addItemCart,
	itemCart,
	addCart,
	removeCart,
	cart,
	counterCart,
	counterFavorites,
}) => {
	return (
		<Switch>
			<Route exact path="/">
				<CardList
					isOpenedModal={isOpenedModal}
					cards={cards}
					handleClickButtonCard={handleClickButtonCard}
					favoriteCards={favoriteCards}
					addFavoriteCards={addFavoriteCards}
					removeFavoriteCard={removeFavoriteCard}
					addItemCart={addItemCart}
					itemCart={itemCart}
					addCart={addCart}
					removeCart={removeCart}
					cart={cart}
				/>
			</Route>
			<Route path="/cart">
				<Cart
					counterCart={counterCart}
					cart={cart}
					removeCart={removeCart}
					isOpenedModal={isOpenedModal}
					handleClickButtonCard={handleClickButtonCard}
					addItemCart={addItemCart}
					itemCart={itemCart}
				/>
			</Route>
			<Route path="/favorites">
				<Favorites
					counterFavorites={counterFavorites}
					favoriteCards={favoriteCards}
					addFavoriteCards={addFavoriteCards}
					removeFavoriteCard={removeFavoriteCard}
				/>
			</Route>
		</Switch>
	);
};
